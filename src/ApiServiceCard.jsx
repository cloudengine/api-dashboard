import React, { Component } from "react";
import axios from "axios";

export default class ApiServiceCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      info: {
        app: {
          name: "",
          description: "",
          version: "",
          java: {
            version: "",
          },
        },
      },
      health: {
        status: "",
      },
    };
  }

  componentDidMount() {
    const baseUrl = this.props.apiItem.actuatorEndpoint;

    this.getDataFromEndpoint(baseUrl, "health", "health");
    this.getDataFromEndpoint(baseUrl, "info", "info");
  }

  getDataFromEndpoint = (baseUrl, endPoint, stateKey) => {
    this.setState(
      {
        [`${stateKey}Loading`]: true,
        [`${stateKey}Error`]: null,
      },
      () => {
        axios
          .get(`${baseUrl}/${endPoint}`)
          .then((res) => {
            // pulled data successfully
            const { data } = res;

            this.setState({
              [`${stateKey}Loading`]: false,
              [`${stateKey}Error`]: null,
              [`${stateKey}`]: data,
            });
          })
          .catch((error) => {
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(error.response.data);
              console.log(error.response.status);
              console.log(error.response.headers);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log("Error", error.message);
            }

            // failure
            this.setState({
              [`${stateKey}Loading`]: false,
              [`${stateKey}Error`]: true,
              [`${stateKey}ErrorString`]: error.message,
            });

            console.log(error.config);
          });
      }
    );
  };

  render() {
    const {
      health,
      healthLoading,
      healthError,
      healthErrorString,
      info,
      infoLoading,
      infoError,
      infoErrorString,
    } = this.state;

    let statusBgClass;
    let statusFgClass;
    let statusMessage;

    let description;
    let version;
    let jdkVersion;

    // while loading, greyed out color scheme
    if (healthLoading || infoLoading) {
      statusBgClass = "bg-secondary";
      statusFgClass = "text-secondary";
      statusMessage = "Details loading...";
    } else if (healthError || infoError) {
      statusBgClass = "bg-danger";
      statusFgClass = "text-danger";
      statusMessage = healthErrorString;
    } else {
      statusBgClass = "bg-success";
      statusFgClass = "text-success";
      description = info.app.description;
      version = "Version: " + info.app.version;
      jdkVersion = "Java version: " + info.app.java.version;
    }

    return (
      <React.Fragment>
        <div className="card">
          <div className={`card-header ${statusBgClass}`}>
            <div className="container">
              <div className="row">
                <h3 className="col-lg-3 text-white">
                  {this.props.apiItem.name}
                </h3>
                <div className="col-lg-6">
                  <ul className="list-unstyled text-white">
                    <li>{description}</li>
                    <li>{version}</li>
                  </ul>
                </div>
                <h6 className="col-lg-3 text-white">{jdkVersion}</h6>
              </div>
            </div>
          </div>

          <div className={`card-body ${statusFgClass}`}>
            <div className="container">
              <div className="row">
                <i className="col-lg-6 fas fa-server fa-10x"></i>
              </div>
            </div>
          </div>

          <div className="card-footer text-muted">
            <h6>{statusMessage}</h6>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
