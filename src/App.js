import React from "react";

import ApiServiceData from "./api-services.json";
import ApiServiceCard from "./ApiServiceCard";

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-9">
            <div className="service-dashboard">
              <ApiServicesSection apiList={ApiServiceData.apis} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ApiServicesSection = ({ apiList }) => {
  return (
    <div>
      {apiList.map((apiItem, index) => (
        <ApiServiceCard key={index} apiItem={apiItem} />
      ))}
    </div>
  );
};

export default App;
